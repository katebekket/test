<?php include "header.php" ?>
<?php include "block-zakaz.php" ?>
	<div class="container breadcrumbs-container">
		<div class="row">
			<div class="col-xs-12">
				<div id="breadcrumbs" class="block-breadcrumbs">
					<span><a title="remontvsem.by" href="#" class="home"><span>Главная</span></a></span> /
					<span><span>Заказы</span></span>
				</div>
			</div>
		</div>
		<section class="main-right-bar page-zakazu">
			<div class="row">
				<div class="col-xs-12">
					<h2 class="page-title">Заказы</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-md-8 col-sm-9 col-xs-12">

					<div class="block-content-page">
						<div class="content">
<?php print_zakazu()?>

							
						</div>
					</div>
					
					
				</div>
				<div class="col-md-4 col-sm-3 col-xs-12">

					<?php include "block-filter.php" ?>
				</div>
			</div>
		</section>
	</div>
<?php include "footer.php" ?>